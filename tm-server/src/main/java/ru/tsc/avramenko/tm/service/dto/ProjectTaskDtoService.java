package ru.tsc.avramenko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.avramenko.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.avramenko.tm.api.service.IConnectionService;
import ru.tsc.avramenko.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyIndexException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.repository.dto.ProjectDtoRepository;
import ru.tsc.avramenko.tm.repository.dto.TaskDtoRepository;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public ProjectTaskDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskDtoRepository = new TaskDtoRepository(entityManager);
            return taskDtoRepository.findAllTaskByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO bindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskDtoRepository = new TaskDtoRepository(entityManager);
            @Nullable final TaskDTO task = taskDtoRepository.findById(userId, taskId);
            task.setProjectId(projectId);
            taskDtoRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO unbindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskDtoRepository = new TaskDtoRepository(entityManager);
            @Nullable final TaskDTO task = taskDtoRepository.findById(userId, taskId);
            task.setProjectId(null);
            taskDtoRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskDtoRepository = new TaskDtoRepository(entityManager);
            @NotNull final IProjectDtoRepository projectDtoRepository = new ProjectDtoRepository(entityManager);
            taskDtoRepository.unbindAllTaskByProjectId(userId, projectId);
            projectDtoRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskDtoRepository = new TaskDtoRepository(entityManager);
            @NotNull final IProjectDtoRepository projectDtoRepository = new ProjectDtoRepository(entityManager);
            @NotNull final String projectId = Optional.ofNullable(projectDtoRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskDtoRepository.unbindAllTaskByProjectId(userId, projectId);
            projectDtoRepository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskDtoRepository = new TaskDtoRepository(entityManager);
            @NotNull final IProjectDtoRepository projectDtoRepository = new ProjectDtoRepository(entityManager);
            @NotNull final String projectId = Optional.ofNullable(projectDtoRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskDtoRepository.unbindAllTaskByProjectId(userId, projectId);
            projectDtoRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}