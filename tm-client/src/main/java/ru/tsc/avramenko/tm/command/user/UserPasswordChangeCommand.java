package ru.tsc.avramenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ICommandService;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class UserPasswordChangeCommand extends AbstractUserCommand {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private UserEndpoint userEndpoint;

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private ICommandService commandService;

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change the user's password.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER USER ID: ");
        @Nullable final String userId = TerminalUtil.nextLine();
        @NotNull final String currentUserId = sessionService.getSession().getUserId();
        if (!userId.equals(currentUserId)) throw new AccessDeniedException();
        @NotNull final boolean userExists = userEndpoint.existsUserById(session, userId);
        if (!userExists) throw new UserNotFoundException();
        System.out.println("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        adminUserEndpoint.updateUserPassword(session, password);
        commandService.getCommandByName("logout").execute();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}