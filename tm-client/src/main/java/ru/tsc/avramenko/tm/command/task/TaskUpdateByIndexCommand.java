package ru.tsc.avramenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber()-1;
        @Nullable final TaskDTO task = taskEndpoint.findTaskByIndex(session, index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final TaskDTO taskUpdated = taskEndpoint.updateTaskByIndex(session, index, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}