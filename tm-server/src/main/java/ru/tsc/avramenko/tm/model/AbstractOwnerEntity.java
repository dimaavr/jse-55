package ru.tsc.avramenko.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public class AbstractOwnerEntity extends AbstractEntity {

    @Nullable
    @ManyToOne
    protected User user;

}