package ru.tsc.avramenko.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ICommandService;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.command.AbstractCommand;

@Component
public class VersionDisplayCommand extends AbstractCommand {

    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @Nullable
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("PROGRAM VERSION: " + propertyService.getApplicationVersion());
        System.out.println("BUILD NUMBER: " + Manifests.read("build"));
    }

}