package ru.tsc.avramenko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.command.AbstractProjectCommand;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class ProjectShowListCommand extends AbstractProjectCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show a list of projects.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        @NotNull List<ProjectDTO> projects;
        projects = projectEndpoint.findProjectAll(session);
        int index = 1;
        for (ProjectDTO project : projects) {
            System.out.println("\n" + "|--- Project [" + index + "]---|");
            showProject(project);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}