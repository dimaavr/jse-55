package ru.tsc.avramenko.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.command.AbstractCommand;

@Component
public class AboutDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @Nullable
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer information.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: " + Manifests.read("developer"));
        System.out.println("E-MAIL: " + Manifests.read("email"));
    }

}